// File: datasource.js
// Name: Lee Grobler
// Date: 07/12/2017
// Time: 19:39
// Desc: Managing the catalogue, saving to and loading from local storage.

let cart = []; // One of the only three global variables, this one holds the user's cart after it's been loaded from local storage
let settings; // One of the only three global vriables, this one holds the settings (i.e. the last viewed tabe) after it's been loaded from local storage
// The products which will be shown in the catalogue, instructions on how to modify this array is available in the ReadMe.
let products = [
  {code:'pt',name:"Pink Mabob",price:54.95,img:['asset/img/5.jpg','asset/img/6.jpg','asset/img/7.jpg']},
  {code:'otss',name:"Old Timy Swim Suit",price:120.00,img:['asset/img/7.jpg','asset/img/5.jpg','asset/img/6.jpg']},
  {code:'gplt',name:"Grayish Headwear",price:78.45,img:['asset/img/6.jpg','asset/img/7.jpg','asset/img/5.jpg']}
];

function load() { // This function loads all the data from local storage
  if(typeof(Storage) === undefined) { // First it checks if the user's browser supports local storage
    // If the local storage is not supported, a message is logged to the browser's console
    // Update this line to show a message on the page, a more appropriate message can also be used
    console.log('Your browser does not support Browser Storage. Please try a more modern browser.');
  } else {
    if(!localStorage.cartVars) { // If local storage has not been set before (i.e. it's the first time using the app) the following happens:
      // local storage gets set (with a JSON string), the productsTab is set as the last viewd tab along with an empty cart
      localStorage.cartVars = '{"settings":{"page":"productsTab"}, "cart":[]}';
      location.reload(); // The page gets refreshed so that the following "else" block gets hit
    } else {
      let data = JSON.parse(localStorage.cartVars); // The locally stored JSON string gets converted to a Javascript object
      cart = data.cart; // The global "cart" variable gets initialised to the cart stored in local storage
      settings = data.settings; // The global "settings" variable gets initialised to the settings stored in local storage

      // Synchronises the details of the items (name, price, img, etc) in the user's cart with the item details of the catalogue,
      // so for example, if I have "Cereal" in my cart @ R12.45 and the product get changed to "Coco Pops" @ R16.95, it updates my cart
      cart.forEach((item, i) => {
        let product = getProductByCode(item.code); // gets the catalogue product based on the cart item's product code
        if(product) { // If the catalogue product exists:
          // This is where changes will also be made if the products' properties gets edited
          item.name = product.name; // Set the cart item's name equal to the catalogue product's name
          item.price = product.price; // Set the cart item's price equal to the catalogue product's price
          item.img = product.img; // Set the cart item's image list equal to the catalogue product's image list
        } else { // If the catalogue proudct doesn't exist:
          remove(item.code); // Remove the product from the user's cart
        }
      });
    }
  }
}

function save(page) { // This function saves all the data to local storage
  if(page) // If a "page" param is received, update the saved last viewed page, otherwise leave it as it was before
    settings.page = page;

  let data = {settings:settings,cart:cart}; // Add the global "cart" and global "settings" variables to a "data" object
  data = JSON.stringify(data); // Convert the "data" object to a JSON string
  localStorage.cartVars = data; // Persist the JSON string to local storage
}

function getProductByCode(code) { // Returns a catalogue product based on the "code" param received
  let product = null; // Define a "product" variable and make sure it's value is "null"
  products.forEach((item, i) => { // Loop through all the products in the catalogue
    if(item.code === code) { // If a catalogue product is encountered with a product code equal to the code param received:
      product = item; // Set the "product" variable to the catalogue product encountered
    }
  });
  return product; // Return the product
}

function getCartItemByCode(code) { // Returns a cart item based on the "code" param received
  let item = null; // Define a "item" variable and make sure it's value is "null"
  cart.forEach((product, i) => { // Loop through the user's cart
    if(product.code === code) { // If an item in the user's cart is found with the same product code as the code param received:
      item = product; // Set the "item" variable to the cart item encountered
    }
  });
  return item; // Return the item
}
