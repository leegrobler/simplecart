// File: main.js
// Name: Lee Grobler
// Date: 30/11/2017
// Time: 20:21
// Desc: The app's Bootstrap and "router".

$(document).ready(() => { // When the page is finished loading:
  load(); // Load all the data from local storage

  if(settings.page) { // If a last viewed tab is found in the global "settings" variable:
    if(settings.page === 'productsTab') { // If the last viewed tab is catalogue tab:
      showCatalogue(); // Show the catalogue tab
    } else if(settings.page === 'cartTab') { // If the last viewed tab is the cart tab:
      showCart(); // Show the user's cart
    } else if(settings.page.indexOf('view') !== -1) { // If the last viewed tab contains the string "view":
      showView(settings.page.split('-')[1]); // Get the product code from the last viewed tab and show the view tab for that product
    }
  } else { // If a last viewed tab is not found (i.e. the global "settings" variable does not exist):
    showCatalogue(); // Show the catalogue tab
  }
});

function showCatalogue() { // Shows the catalogue tab
  $('.viewTab, .cartTab').hide('active', () => { // Hides the view tab and cart tab
    let htmlString = '<div class="row">'; // Declares HTML string, which will be put in a parent div
    // Loops through all the products in the catalogue and creates HTML for them
    // This part can be edited for styling purposes, but observe how the products' details gets shown to the user
    // If the catalogue products' properties gets updated, changes will have to be made here, a proper description on how to do so can be found in the ReadMe
    products.forEach((product, i) => {
      htmlString += '' +
        '<div class="col-md-4 text-center product-square">' +
          '<img class="product-img" id="img-'+product.code+'" src="'+product.img[0]+'" />' +
          '<div class="product-price" id="price-'+product.code+'">R'+product.price.toFixed(2)+'</div>' +
          '<div class="product-name" id="name-'+product.code+'">'+product.name+'</div>' +
          '<button class="product-view-btn" id="view-btn-'+product.code+'" onclick="showView(\''+product.code+'\')">' +
            '<i class="fa fa-eye" aria-hidden="true"></i>' +
          '</button>' +
          '<button class="product-cart-btn" id="cart-btn-'+product.code+'" onclick="add(\''+product.code+'\')">' +
            '<i class="fa fa-shopping-cart" aria-hidden="true"></i>' +
          '</button>' +
        '</div>';
    });
    htmlString += '</div>'; // Closes of the parent div in the HTML string

    $('#productsGoHere').html(htmlString) // Sets the "#productsGoHere" div's HTML to the above created HTML string
    $('.catalogueTab').show('active'); // Shows the catalogue tab
    save('productsTab'); // Saves the catalogue as the last viewed tab
  });
}

function showView(code) { // Shows the view product tab for the product code received through the "code" param
  product = getProductByCode(code); // Retrieves the catalogue product based on the "code" param
  if(product !== null && product !== undefined) { // If the catalogue product exists:
    $('.catalogueTab, .cartTab').hide('active', () => { // Hides the catalogue and cart tabs
      // Declares a HTML string variable, this part can be edited but take note of how the various properties of the product gets displayed to the user
      let htmlString = '' +
        '<div class="product-window">' +
          '<div class="view-img">' +
            '<img class="view-main-img" id="img-'+product.code+'" src="'+product.img[0]+'" />';

      product.img.forEach((img, i) => { // This part loops through the product's image list and adds them to the HTML string
        htmlString += '<img class="view-extra-img" src="'+img+'" />';
      });

      htmlString += '' + // Finishes up the HTML string
          '</div>' +
          '<div>' +
            '<h3>'+product.name+'</h3>' +
            '<h4>R'+product.price.toFixed(2)+'</h4>' +
            '<button onclick="add(\''+product.code+'\')"><i class="fa fa-shopping-cart" aria-hidden="true"></i></button>' +
          '</div>' +
        '</div>';

      $('#viewGoesHere').html(htmlString); // Sets the "#viewGoesHere" div's HTML to the HTML string created above
      $('.viewTab').show('active'); // Shows the view tab
    });

    save('view-'+product.code); // Saves the last viewed tab as the view product tab along with the currently viewed product
  } else { // If the product is not found:
    // Logs a message to the user alerting him that the product doesn't exist
    // This part should be updated to show the message on the page with a more appropriate message
    console.log('Product not found!');
    showCatalogue(); // Shows the catalogue tab
  }
}

function showCart() { // Shows the user's cart
  $('.catalogueTab, .viewTab').hide('active', () => { // Hides the catalogue and view product tabs

    let total = 0; // Declares a variable that holds the total cost of all the items in the user's cart
    let htmlString = '<div class="row">'; // Declares the variable that will hold the HTML that the user sees
    // Loops through all the products in the user's cart and adds them to the HTML string
    cart.forEach((product, i) => {
      total += product.price * product.quantity; // Adds the product's price x the product's quantity to the total
      // The HTML in this string can be edited but take note of how the products' properties are displayed to the user
      // A detailed description of how to update products's properties can be found in the ReadMe
      htmlString += '' +
        '<div class="col-md-6 col-md-offset-3 cart-item">' +
          '<img class="cart-img" id="img-'+product.code+'" src="'+product.img[0]+'" />' +
          '<div class="section1">' +
            '<p>'+product.name+'</p>' +
            '<p>R'+product.price.toFixed(2)+'</p>' +
          '</div>' +
          '<div class="section2">' +
            '<button type="button" onclick="increase(\''+product.code+'\')"><i class="fa fa-plus" aria-hidden="true"></i></button>' +
            '<span class="item-qnt-label">'+product.quantity+'</span>' +
            '<button type="button" onclick="decrease(\''+product.code+'\')"><i class="fa fa-minus" aria-hidden="true"></i></button>' +
          '</div>' +
          '<div class="section3">' +
            '<button type="button" onclick="remove(\''+product.code+'\')"><i class="fa fa-trash" aria-hidden="true"></i></button>' +
          '</div>' +
        '</div>';

    });

    // Adds the grand total to the HTML string, the HTML can be updated but take note of how the total gets added to the HTML
    htmlString += '' +
        '<div class="col-md-6 col-md-offset-3 cart-total">' +
          '<h3><span class="pull-left">Total</span> <span class="pull-right">R'+total.toFixed(2)+'</span></h3>' +
        '</div>' +
      '</div>';

    $('#itemsGoHere').html(htmlString); // Sets the "#itemsGoHere" div's HTML to the HTML string created above
    $('.cartTab').show('active'); // Shows the cart tab
    save('cartTab'); // Saves the cart tab as the last viewed tab
  });
}


























//
