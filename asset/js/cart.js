// File: cart.js
// Name: Lee Grobler
// Date: 07/12/2017
// Time: 21:55
// Desc: Performs operations on the contents of the user's cart.

function add(code) { // Adds a product to the user's cart
  let product = getProductByCode(code); // Gets a product with a product code equal to the code param received
  if(product !== null && product !== undefined) { // If a product is returned from "getProductByCode":

    let alreadyInCart = false; // Define a variable used to monitor whether the item is already in the user's cart
    cart.forEach((item, i) => { // Loops through the user's cart
      if(item.code === product.code) { // If an item in the user's cart is encountered having the same product code as the product to be added:
        alreadyInCart = true; // Reassign the monitor variable to "true"
      }
    });

    if(!alreadyInCart) { // If the monitor variable is set to false (i.e. the product is not already in the user's cart):
      // Add an item to the user's cart with all the same properties as the selected product, plus one extra property; "quantity"
      // If the catalogue products' properties get updated, a change, described in the ReadMe, will have to be made here
      cart.push({code:product.code,name:product.name,price:product.price,quantity:1,img:product.img});
      save(); // Saves the user's cart
    } else { // If the monitor variable is set to true (i.e. the product is already in the user's cart):
      // Log a message telling the user the product is already in his/her cart
      // This should be updated to a more appropriate message and displayed in an appropriate manner
      console.log('Already in cart!');
    }

  } else { // If a product is not returned from "getProductByCode"
    // Log a message telling the user the product doesn't exist
    // This should be updated to a more appropriate message and displayed in an appropriate manner
    console.log('Product not found!');
  }
}

function remove(item) { // Removes a item from the user's cart
  item = getCartItemByCode(item); // Retrieves an item from the user's cart based on the "item" (product code) param received
  if(item !== null && item !== undefined) { // If the item is found:
    let indexOfItem = cart.indexOf(item); // Get the number of the item in the cart array (defined as the INDEX)
    cart.splice(indexOfItem, 1); // Removes the item from the array
    save(); // Saves the user's updated cart
    showCart(); // Refreshes the cart tab
  } else { // If the item is not found
    // Log a message telling the user the isn't in his/her cart
    // This should be updated to a more appropriate message and displayed in an appropriate manner
    console.log('Item not found!');
  }
}

function increase(item) { // Increments the item in the user's cart (increases it's quantity by 1)
  item = getCartItemByCode(item); // Retrieves an item from the user's cart based on the "item" (product code) param received
  if(item !== null && item !== undefined) { // If the item is found:
    item.quantity++; // Increment the item's quantity property (increase it by 1)
    save(); // Saves the user's updated cart
    showCart(); // Refreshes the cart tab
  } else { // If the item is not found
    // Log a message telling the user the isn't in his/her cart
    // This should be updated to a more appropriate message and displayed in an appropriate manner
    console.log('Item not found!');
  }
}

function decrease(item) { // Decrements the item in the user's cart (decreases it's quantity by 1)
  item = getCartItemByCode(item); // Retrieves an item from the user's cart based on the "item" (product code) param received
  if(item !== null && item !== undefined) { // If the item is found:
    item.quantity--; // Increment the item's quantity property (increase it by 1)
    if(item.quantity <= 0) { // If the item's quantity is zero or less (it should never be able to go below zero):
      remove(item.code); // Remove it from the user's cart
    } else { // If the item's quantity is more than zero:
      save(); // Saves the user's updated cart
      showCart(); // Refreshes the cart tab
    }
  } else { // If the item is not found
    // Log a message telling the user the isn't in his/her cart
    // This should be updated to a more appropriate message and displayed in an appropriate manner
    console.log('Item not found!');
  }
}
























//
