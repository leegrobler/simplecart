# simplecart v1.2.0

This is a extremely basic proof of concept cart application **that I'm writing for a friend** so I tried to keep the HTML generating Javascript as close to what you'd expect pure HTML would look like.

This is a super simple frontend signle page cart application which allows the easy configuration of a list of products before-hand in Javascript which will act as a catalogue but the hard-coded product list can easily be swopped out for an API call. The user will then be able to add and remove the products from his/her cart. The cart (along with the last viewed page) gets stored in local storage as a JSON string. It has a 'synchronisation' mechanic so that if a product's details gets changed in the catalogue, it will compare all the products in the user's cart to the products in the catalogue and update the cart with the latest product information.

## How it works

The app consists of 1 HTML file and 3 Javascript files:

### index.html:

As the only HTML page in the app, it serves primarily as an anchor for the three tabs (Catalogue, View Product and Cart). Currently it uses Bootstrap 3's container, rows and columns for the layout but that can easily be changed without affecting the logic or breaking the app. What should not be removed or changed are the actual 'anchors' and their hierarchies. These are the classes *.catalogueTab*, *.viewTab* and *.cartTab* and each of them has 1 child div into which data gets loaded. *.catalogueTab* has the child *#productsGoHere*, *.viewTab* has *#viewGoesHere* and *.cartTab* has *.itemsGoHere*. The order of the anchors can be changed as well as any other HTML that they contain or are surrounded by so long as they keep their class names and keep one child div with the appropriate id.

Notice that each anchor also contains one or more buttons. These serve as navigation buttons that will direct the user between the three tabs. The buttons' classes, ids and text can be edited and they can be traded out between the anchors (which wouldn't really make sense) but to keep them functioning as they should their *onclick* attributes should not be altered.

At the bottom of the page the 3 Javascript files containing all the functionality get loaded in. To be safe, do not change the order in which these files get loaded and always keep them as the last scripts of the page, so if you'd like to load in more Javascript files, do it above these three files.

Appart form the 3 Javascript files the only other dependancies are Jquery, Bootstrap and Font-Awesome. Bootstrap and Font-Awesome can be removed but without Jquery the app will not work so do not remove it.

One custom CSS file (main.css) is loaded into the head and currently serves as the only stylesheet. More can be added.

### datasource.js

This file serves as the data-layer of the application. Starting out, the first three statements declare the variables that will hold the user's cart, the *settings* (which really only holds the last viewed page but can be expanded upon later) and the catalogue, declared as *products*. The only part of this file that should/can be edited is the products. If you want to add/update/remove proudcts from the catalogue do it as follows and be very careful to stay true to this format. The values in and including the angle bracktes *< >* should be exchanged with actual values but keep single quotes where they are (*price* currently being the only property which doesn't have a value in single quotes). The first example shows the format of a product and the second shows an example.

*{code:'<productCode>',name:'<productName>',price:<productPrice>, img:['<pathToFirstImage>','<pathToSecondImage>','<pathToThirdImage>']},*
*{code:'dr-br',name:'Beer',price:64.95, img:['asset/img/sixpack0.jpg','asset/img/sixpack1.jpg','asset/img/sixpack2.jpg']}*

The example above can be exchanged or inserted into the products array (after substituting the values in angle brackets *< >*) but make sure not to touch the leading *let products = [* and trailing *];*. Also, unless the product you insert is the last product in the array, put a comma , after the closing curly brace *}* . Alternatively you can do away with hard-coding the products in the JS file and use an API to load in your own products. **Important**: Once a product has been added to the products array and the app gets redeployed it's product code should not be changed unless the whole product gets removed from the catalogue. The code that compares the user's cart to the catalogue uses the product code for comparrison and if the product code has been changed it won't be able to update the product in the user's cart. The product code should also always be unique.

The rest of the file consists of various functions used to retrieve and store data. The only parts of these functions that can/should be edited is anywhere that a *console.log* is found, in which case a different message can be displayed more appropriately. A short explanation of what each of the functions do:

**load()**: Retrieves the users cart and last viewed page from local storage after determining that the user's browser supports local storage. If it doesn't an appropriate message is logged to the console. If it's the first time the user opens the app, i.e. local storage has not yet been set, a *settings* object and an empty *cart* array gets stored in local storage as a JSON string and the page gets reloaded. After retrieving and parsing the JSON data the user's cart and last viewed page gets initialised. Thereafter it compares the user's cart to the products in the catalogue, updating the user's cart to the correct values (currently *name*, *price* and image list as *img*) in the catalogue. For example in the beginning the catalogue might contain a product named "Beer" priced at R49.00 and a user adds it to his/her cart and then comes back to the application in 3 months when "Beer" might have been renamed to "Black Label" priced at R65.00. So unless this check exists, the user will continue having "Beer" in his cart instead of the updated "Black Label".

**save(page)**: Saves the user's cart and optionally the currently viewed page if a *page* parameter is sent.

**getProductByCode(code)**: Retrieves a catalogue product based on the *code* received.

**getCartItemByCode(code)**: Essentially the same as the previous function except that this one queries the user's cart instead of the catalogue.

### cart.js

This file contains all the operations that can be performed on the items in the user's cart. Note that this file does not actually display the cart, it only adds and removes items and increments and decrements the quantities of items in the user's cart. Similar to datasource.js, the only parts of this file that should be edited is the *console.log* statements which should be exchanged with more appropriate messages and ways of displaying the messages. A brief rundown of the functions in this file:

**add(code)**: Adds a product to the user's cart. First it checks if the catalogue contains a product with the same code as the code received and if not shows the user a message that the product doesn't exist. If the product is found it checks if the product is already in the user's cart and if it is a message is logged that the user already has the product in his/her cart. If the product is not alreay in the user's cart it gets added to the cart along with a *quantity* property which gets set to 1. Lastly the cart gets saved to local storage.

**remove(item)**: Removes a product from the user's cart. First the item to remove is retrieved from the *getCartItemByCode* function, if it's not found a message is logged in the console otherwise it's removed, the cart is saved to local storage and the cart is refreshed. The reason that the cart is refreshed is to avoid having to update the cart manually which would require more code that could interfere with the HTML feel we're trying to go for.

**increase(item)**: Increases the *quantity* of the item in the cart by 1. It gets the item from from the *getCartItemByCode* function based on the *item* parameter received (which is really a product code masquerading as an item xD) and if the item is not found a message is logged. Otherwise the item's *quantity* property is incremented by 1, the cart is saved to local storage and the cart tab is refreshed.

**decrease(item)**: The exact opposite of the *increase* function, with the only exception being that if the item's quantity reaches zero or below it gets removed from the cart by means of the *remove* function.

### main.js

This file acts as both a "bootstrap" for the app and a "controller" which directs the user between the three views (tabs). This file displays the tabs through dynamically creating HTML based on the products available and the user's actions (such as clicking the "View Product", "Cart" or "Home" button). This is where changes can be made to affect the styling of the app but do so with caution, being careful to observe the hierarchies of the html and the class names and ids of the elements as well as the Javascript variables and/or expressions injected into the HTML. Staring from the top, explanations of the various functions:

**$(document).ready()**: This is the "bootstrap" part of the file, it starts by loading in the data from local storage and displaying the appropriate tab based on the settings stored in said data. If no settings have been stored yet, it displays the catalogue tab.

**showCatalogue()**: First it hides the two other tabs before it proceeds to generate an HTML string based on the products available whereafter it injects the HTML string into the *#productsGoHere* div, shows the catalogue tab and saves the catalogue tab as the last viewed tab.

**showView(code)**: This one starts by retrieving the product based on the *code* param received then similarly to the *showCatalogue* function, it hides the other two tabs, generates an HTML string for the product, injects the HTML string into the *#viewGoesHere* div, shows the view tab and saves the specific product view tab as the last viewed tab. If the product could not be found, a message will be displayed and the catalogue tab will be displayed instead.

**showCart()**: Similar to the *showCatalogue* function, *showCart* starts by hiding the the other two tabs, then it declares a *total* variable which will store the total price off all the items in the user's cart, after which it will generate the HTML string based on the products in the user's cart. After that a following bit of HTML gets generated, which shows the total price of all the items in the user's cart. Lastly it injects the HTML into the *#itemsGoHere* div, shows the cart tab and saves the cart tab as the last viewed tab.

**A point on styling and design**

Although the class names, ids and even the HTML tags and attributes can be edited it by no means has to, as the app was designed from the beginning with styling in mind and the current identifiers should be more than sufficient to handle all styling requirements. Furthermore, the original intent was to use hierarchical styling, for example the products tab might have 3 product entries, each contained in a div with id *#product-square* and each product square might have an image identified by id *#product-img* so to change the styling of *#product-img* the following rule would be added to the CSS page: *#product-square #product-img {}* instead of directly referencing like this *#product-img {}*. Although this styling stategy does not **have** to be adhered to, it is strongly recommended because it will make the CSS much more readable and maintainable.

**A point on products and their properties**

The app was originally created with products in mind that would only have 4 properties each (namely *code*, *name*, *price* and *img*) and adding or removing properties might seem daunting, doing so is actually quite simple. First off, always stay consistant with properties, so if you add/remove/update a property for one product it **has** to be updated for all other products.

To add a property will be the simplest because it requires the least changes to the rest of the code, simply add it to each of the products and it's done. For example, to add the property *type* to the previous "Beer" example, the product will change to the following:

*{code:'dr-br',name:'Beer',price:64.95, img:['asset/img/sixpack0.jpg','asset/img/sixpack1.jpg','asset/img/sixpack2.jpg']}  // the original example*
*{code:'dr-br',name:'Beer',price:64.95, img:['asset/img/sixpack0.jpg','asset/img/sixpack1.jpg','asset/img/sixpack2.jpg'], type:'Alcohol'}  // Updated to include the "type" property.*

Do note that doing the above is not enough to add the property to what the user sees. To do so will require updating the cart.js file's *add* function, updating the dynamic HTML generated by this file and updating the synchronisation mechanic. To update the *add* function, in the cart.js/*add* there's a line starting with *cart.push*, in this line's curly braces simply add *type:product.type* after the last property. An example of updating the catalogue tab would be to add the following div tag directly below the *.product-name* div tag.

*'<div class="product-type" id="product-type">'+product.name+'</div>' +*

The above is only an example and any HTML, class names and ids can be used to update the catalogue but do take note of the *'+product.type+'* section, this is where the new *type* property gets displayed. All other dynamic HTML in this file display properties like this, i.e. *object-name.object-property*. Lastly the synchronisation mechanic should be updated to check for the new property. In datasource.js, in the *load* function, in the code that loops through the user's cart, there are several lines of code that compares the user's cart to the catalogue, which look like this: *item.name = product.name;*. Simply add the following new line below the last check: *item.type = product.type;* (assuming that *type* is the newly added property. If it's not exchange both instances of *type* with the newly added property).

To remove a property simply follow the steps to add a property in reverse. An easier way will be to only remove the property from the dynamic HTML without removing it from the products in the *products* array but to completely remove it, read on. First of all remove the check from the synchronisation mechanic, so if you wanted to remove the *name* property, simply remove the line *item.name = product.name;*. Thereafter find all instances of the property in the dynamic HTML and remove them, for *name* you would have to remove the following line in this file's *showCatalogue* funtion:

*'<div class="product-name" id="name-'+product.code+'">'+product.name+'</div>' +*

The following line from the *showView* function:

*'<h3>'+product.name+'</h3>' +*

And the following line from the *showCart* function:

*'<p>'+product.name+'</p>' +*

After removing the property from all the HTML, remove it from datasource.js' *add* function by removing the updating the *cart.push* line to not include *name:product.name*. Once this is done it is safe to remove the property from the actual product.

## Repo owner

To get in contact with me feel free to drop me an email at seriouslee131@gmail.com. 
